#include <iostream>
#include <iomanip>

using namespace std;
const int n = 5;
int d = 31;
int v,s = 0;

int main() {
    int  a[10][10];
    v = d % n;
    for ( int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            a[i][j] = i + j;
            cout << setw(5) << a[i][j];
            if (v == i)
            {
                s += a[i][j];
            }
        }
        cout << endl;
    }
    cout << "\nSumma i = " << s << '\n';
}